package com.srspiteri.demo.networkManager.model.commitResponse

import com.google.gson.annotations.SerializedName

/**
 * Data class nested under [GitCommitResponse]
 */
data class GitUser(

    @SerializedName("id")
    val id: Long?,

    @SerializedName("login")
    val login: String?,

    @SerializedName("avatar_url")
    val avatarUrl: String?,

    @SerializedName("url")
    var url: String?
)
