package com.srspiteri.demo.networkManager.model.commitResponse.gitCommit

import com.google.gson.annotations.SerializedName

/**
 * Data class object nested under [GitCommit]
 */
data class GitCommitUser(

    @SerializedName("name")
    val name: String?,

    @SerializedName("email")
    val email: String?,

    // TODO : ISO 8601 date format
    @SerializedName("date")
    val date: String?
)