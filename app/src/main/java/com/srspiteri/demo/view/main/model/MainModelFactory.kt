package com.srspiteri.demo.view.main.model

import android.app.Application
import com.srspiteri.demo.networkManager.NetworkManager

/**
 * Factory pattern for decoupling [Application] from [MainModel]
 */
object MainModelFactory {

    fun create(application: Application) : MainModel{
        return MainModel(NetworkManager.getInstance(application))
    }
}