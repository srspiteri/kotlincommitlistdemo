package com.srspiteri.demo.view.recyclerView

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
//import com.bumptech.glide.Glide
import com.srspiteri.demo.R
import com.srspiteri.demo.networkManager.model.commitResponse.GitCommitResponse
import com.srspiteri.demo.view.recyclerView.viewHolder.GitCommitListItemViewHolder
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat


class GitCommitListItemRecyclerViewAdapter(private val items : List<GitCommitResponse>) : RecyclerView.Adapter<GitCommitListItemViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GitCommitListItemViewHolder {

        val view: View = LayoutInflater.from(parent.context)
            .inflate(R.layout.layout_git_commit_list_item, parent, false)

        return GitCommitListItemViewHolder(view)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holderGit: GitCommitListItemViewHolder, position: Int) {

        val item = items[position]

        holderGit.commitMessageTv.text = item.commit?.message
        holderGit.committerNameTv.text =
            String.format(holderGit.committerNameTv.resources
                .getString(R.string.committed_by_format), item.committer?.login)

        Glide.with(holderGit.committerAvatarIv.context)
            .load(item.committer?.avatarUrl)
            .centerCrop()
            .into(holderGit.committerAvatarIv)

        // Convert the ISO 8601 format date into a user facing date string
        // We are using JODA time because it's easier than the outdated calendar pattern.
        // We can use the more modern LocalDate but it wont compile on lower Android versions (v26+ only)
        val dateTime = DateTime(item.commit?.committer?.date)
        val dtf = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss")
        holderGit.commitTimeStamp.text = dtf.print(dateTime)
    }
}