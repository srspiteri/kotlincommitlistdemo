package com.srspiteri.demo.networkManager.model.commitResponse.gitCommit

import com.google.gson.annotations.SerializedName

/**
 * Data class nested under [GitCommit]
 */
data class GitCommit(

    @SerializedName("author")
    val author: GitCommitUser?,

    @SerializedName("committer")
    val committer: GitCommitUser?,

    @SerializedName("message")
    val message: String?
)