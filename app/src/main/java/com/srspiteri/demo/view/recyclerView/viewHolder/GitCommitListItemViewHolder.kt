package com.srspiteri.demo.view.recyclerView.viewHolder

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.srspiteri.demo.R

class GitCommitListItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    val committerNameTv: TextView = view.findViewById(R.id.git_commit_list_item_committer_tv)
    val committerAvatarIv: ImageView = view.findViewById(R.id.git_commit_list_item_committer_avatar_iv)
    val commitMessageTv: TextView = view.findViewById(R.id.git_commit_list_item_commit_message_tv)
    val commitTimeStamp : TextView = view.findViewById(R.id.git_commit_list_item_commit_time_stamp_tv)
}