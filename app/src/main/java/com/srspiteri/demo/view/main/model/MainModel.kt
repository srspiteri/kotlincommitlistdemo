package com.srspiteri.demo.view.main.model

import com.android.volley.Response
import com.android.volley.toolbox.JsonArrayRequest
import com.srspiteri.demo.networkManager.NetworkManager
import com.srspiteri.demo.networkManager.model.commitResponse.GitCommitResponse

/**
 * Main model
 *
 * For main screen business logic.
 */
class MainModel(private val networkManager: NetworkManager) {

    private var request : JsonArrayRequest? = null

    fun getGitCommitResponses(
        onSuccess: Response.Listener<List<GitCommitResponse>>,
        onFailure: Response.ErrorListener
    ) {

        // Declare the request
        val request = networkManager.getGitCommitResponsesRequest(onSuccess, onFailure)

        // Queue the request
        networkManager.addToRequestQueue(request)

        // Update the request so we can cancel it if needed
        this.request = request
    }

    fun cancelRequest(){
        networkManager.cancelAll(request)
    }
}