package com.srspiteri.demo.networkManager

import android.content.Context
import com.android.volley.*
import com.android.volley.toolbox.*
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.srspiteri.demo.networkManager.model.commitResponse.GitCommitResponse
import com.google.gson.reflect.TypeToken

/**
 * Network manager for getting data from git hub api
 * @param applicationContext
 */
class NetworkManager(applicationContext: Context) {

    companion object {

        private const val gitHubHost = "https://api.github.com"

        private const val getCommitsRequestEndPoint = "$gitHubHost/repos/JetBrains/kotlin/commits"

        // Declare singleton instance variable
        @Volatile
        private var INSTANCE: NetworkManager? = null

        /**
         * Get network instance singleton
         * @param applicationContext
         */
        fun getInstance(applicationContext: Context): NetworkManager {

            // Return the instance or create a new one in sync
            return INSTANCE ?: synchronized(this) {
                INSTANCE ?: NetworkManager(applicationContext).also {

                    // Set the instance so we can retrieve it again later
                    INSTANCE = it
                }
            }
        }
    }

    // Declare Cache
    private val cache: Cache by lazy {
        DiskBasedCache(applicationContext.cacheDir, 1024 * 1024) // 1MB cap
    }

    // Declare network with HttpUrlConnection as the http client
    private val network: Network by lazy {
        BasicNetwork(HurlStack())
    }

    // Declare request requestQueue with cache and network
    private val requestQueue: RequestQueue by lazy {

        RequestQueue(cache, network).apply {
            // Start the dispatchers in the requestQueue
            start()
        }
    }

    // Declare gson for serialising json into model objects
    private val gson: Gson by lazy {

        val gsonBuilder = GsonBuilder()
        gsonBuilder.create()
    }

    /**
     * Add a request to the queue
     * @param request
     */
    fun <T> addToRequestQueue(request: Request<T>) : RequestQueue {
        requestQueue.add(request)

        return requestQueue
    }

    /**
     * Cancel request
     * @param tag
     */
    fun <T> cancelAll(tag: Request<T>?) : RequestQueue {

        if (tag != null) {
            requestQueue.cancelAll(tag)
        }

        return requestQueue
    }

    /**
     * Get git commit responses via request
     * @param onSuccess
     * @param onFailure
     */
    fun getGitCommitResponsesRequest(
        onSuccess: Response.Listener<List<GitCommitResponse>>,
        onFailure: Response.ErrorListener
    ): JsonArrayRequest {

        return JsonArrayRequest(
            Request.Method.GET, getCommitsRequestEndPoint, null,

            // Handle success
            Response.Listener {

                // We turn the json array response into a typed list

                // Declare type of object we want to serialise
                val listType = object : TypeToken<List<GitCommitResponse>>() {}.type

                // De-serialise the list of results
                val results: List<GitCommitResponse> = gson.fromJson(it.toString(), listType)

                // Return the result using the listener
                onSuccess.onResponse(results)
            },

            // Handle failure
            onFailure
        )
    }
}