package com.srspiteri.demo

import android.app.Application
import com.srspiteri.demo.networkManager.NetworkManager
import com.srspiteri.demo.view.main.model.MainModelFactory
import org.junit.Test

import org.junit.Assert.*
import org.junit.Before
import org.mockito.Mock
import org.mockito.MockitoAnnotations


class MainActivityUnitTest {

    @Mock
    private lateinit var contextMock: Application

    @Before
    fun init(){
        MockitoAnnotations.initMocks(this)
    }

    @Test
    fun networkManagerNotNull() {

        // Declare the mock network manager
        val mockNetworkManager = NetworkManager.getInstance(contextMock)

        // Is it non-null?
        assertNotNull(mockNetworkManager)
    }

    @Test
    fun mainModelNotNull() {

        // Declare the mock main model
        val mockMainModel = MainModelFactory.create(contextMock)

        // Is it non-null?
        assertNotNull(mockMainModel)
    }
}
