package com.srspiteri.demo.networkManager.model.commitResponse

import com.google.gson.annotations.SerializedName
import com.srspiteri.demo.networkManager.model.commitResponse.gitCommit.GitCommit

data class GitCommitResponse(

    @SerializedName("author")
    val author: GitUser?,

    @SerializedName("committer")
    val committer: GitUser?,

    @SerializedName("commit")
    val commit : GitCommit?
)