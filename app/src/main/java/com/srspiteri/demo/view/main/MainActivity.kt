package com.srspiteri.demo.view.main


import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Response
import com.srspiteri.demo.view.recyclerView.GitCommitListItemRecyclerViewAdapter
import kotlinx.android.synthetic.main.activity_main.*
import androidx.recyclerview.widget.DividerItemDecoration
import com.srspiteri.demo.R
import com.srspiteri.demo.view.main.viewModel.MainViewModel


class MainActivity : AppCompatActivity() {


    private lateinit var viewModel : MainViewModel

    override fun onCreate(savedInstanceState: Bundle?) {

        // Implement parent
        super.onCreate(savedInstanceState)

        // Set the content layout
        setContentView(R.layout.activity_main)

        // Create the view model
        viewModel = ViewModelProvider.AndroidViewModelFactory.getInstance(application)
            .create(MainViewModel::class.java)
    }

    override fun onResume() {

        // Implement parent
        super.onResume()

        // Bind views
        bind()
    }

    /**
     * Bind the views in the layout
     */
    private fun bind() {

        // Declare the recycler view
        val recyclerView = main_activity_recycler_view

        // Declare the progress bar
        val progressBar = main_activity_progress_bar

        // Set layout manager
        val layoutManager = LinearLayoutManager(this)
        recyclerView.layoutManager = layoutManager

        // Set item decoration
        val dividerItemDecoration = DividerItemDecoration(
            recyclerView.context,
            layoutManager.orientation
        )
        recyclerView.addItemDecoration(dividerItemDecoration)

        // Show the progress bar
        progressBar.visibility = View.VISIBLE

        // Get the commit responses
        viewModel.getGitCommitResponses(Response.Listener { gitCommitResponses ->

            // Hide error
            main_activity_error_tv.visibility = View.GONE

            // Update recycler view
            recyclerView.adapter = GitCommitListItemRecyclerViewAdapter(gitCommitResponses)
            recyclerView.visibility = View.VISIBLE

            // Hide progress bar
            progressBar.visibility = View.GONE

        }, Response.ErrorListener {

            // Show error tv
            recyclerView.visibility = View.GONE
            main_activity_error_tv.visibility = View.VISIBLE
            main_activity_error_tv.text = it.message

            // Hide progress bar
            progressBar.visibility = View.GONE
        })
    }

    override fun onPause() {

        // Implement parent
        super.onPause()

        // Handle pause
        viewModel.onPause()
    }
}
