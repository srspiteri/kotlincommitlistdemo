package com.srspiteri.demo.view.main.viewModel


import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.android.volley.Response
import com.srspiteri.demo.networkManager.model.commitResponse.GitCommitResponse
import com.srspiteri.demo.view.main.model.MainModelFactory

/**
 * Main view model
 *
 * We use [AndroidViewModel] to get the application for the [MainModelFactory]
 */
class MainViewModel(application: Application) : AndroidViewModel(application) {

    private val mainModel = MainModelFactory.create(application)

    fun getGitCommitResponses(
        onSuccess: Response.Listener<List<GitCommitResponse>>,
        onFailure: Response.ErrorListener
    ) {
        mainModel.getGitCommitResponses(onSuccess, onFailure)
    }

    fun onPause(){
        // Cancel on-going requests
        mainModel.cancelRequest()
    }
}